let game24 = require('../../lib/game24')

const game24Handler = async (req, res) => {
    // let numbers = req.body.numbers
    try {
        const result = game24(req.body)
        res.status(200).send(result)
    } catch (error) {
        res.status(400).send(error)
    }
}

module.exports = {
    game24Handler
}
// example
// var myHeaders = new Headers();
// myHeaders.append("Content-Type", "application/json");

// var raw = JSON.stringify({"numbers":[7,7,7,7]});

// var requestOptions = {
//   method: 'POST',
//   headers: myHeaders,
//   body: raw,
//   redirect: 'follow'
// };

// fetch("localhost:8081/jenosize/game24", requestOptions)
//   .then(response => response.text())
//   .then(result => console.log(result))
//   .catch(error => console.log('error', error));