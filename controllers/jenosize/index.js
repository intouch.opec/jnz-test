const express = require('express')
const router = express.Router()
const {searchPlaceHandler} = require("./searchPlace")
const {game24Handler} = require("./game24")

router.post('/search_place', searchPlaceHandler)
router.post('/game24', game24Handler)

module.exports = router