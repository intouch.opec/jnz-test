let googleSearchPlace = require('../../lib/googleSearchPlace')

const searchPlaceHandler = async (req, res) => {
    try {
        let result = await googleSearchPlace(req.body)
        res.status(200).send(result)        
    } catch (error) {
        res.status(400).send(error)        
    }
}

module.exports = {
    searchPlaceHandler
}

// var myHeaders = new Headers();
// myHeaders.append("Content-Type", "application/json");
// location: [lati, longti]
// var raw = JSON.stringify({"location":[13.737058 ,100.564435], "radius": 1500}); 

// var requestOptions = {
//   method: 'POST',
//   headers: myHeaders,
//   body: raw,
//   redirect: 'follow'
// };

// fetch("localhost:8081/jenosize/search_place", requestOptions)
//   .then(response => response.text())
//   .then(result => console.log(result))
//   .catch(error => console.log('error', error));