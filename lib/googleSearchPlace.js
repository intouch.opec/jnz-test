let https = require("https")
const urlGoogle = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=:key"

const googleSearchPlace = data => {
    let url = urlGoogle.replace(":key", process.env.API_KEY_GOOGLE_MAP || "AIzaSyDy6RiAcYuyd0mmuIczBJ84u7Wgvc_dqQw")
    if (data.type === undefined) {
        data.type = "restaurant"
    }
    if (data.sensor === undefined) {
        data.sensor = false
    }
    if (data.radius === undefined) {
        data.radius = process.env.INIT_RADIUS || 1500
    }
    if (!data.location) {
        data.location = [process.env.INIT_LATI || 13.893983, process.env.INIT_LONGTI || 100.5141033]
    }
    Object.keys(data).forEach(index => {
        if (Array.isArray(data[index])) {
            url += "&" + index + "=" + encodeURIComponent(data[index])
        } else {
            url += "&" + index + "=" + data[index]
        }
    })
    console.log(url)
    return new Promise((res, rej) => {
        https.get(url, response => {
            let body = ""
            response.on("data", (chunk) => {
                body += chunk
            })
            response.on("end", function() {
                let places = JSON.parse(body)
                res(JSON.parse(body))
            })
        }).on("error", e => {
            rej(e)
        })
    })
}

const fields = [
    "location",
    "radiu",
    "radius",
    "sensor",
    "types",
    "keyword",
    "fields",
    "locationbias",
    "pagetoken",
    "type"
]

module.exports = async data => {
    let fv = [...fields]
    let field = ""
    for (const key in data) {
        field = fv.find((val, i) => val === key)
        if (!field) {
            throw `${field}`
        }
    }
    return await googleSearchPlace(data) 
}
