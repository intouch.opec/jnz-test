const game24 = numbers => {
    if (numbers.length == 1) {
        return Math.abs(numbers[0] - 24) < 0.01
    }

    let ans = false
    for (let i = 0; i < numbers.length; ++i) {
        for (let j = 0; j < i; ++j) {
            const rest = [];
            for (let k = 0; k < numbers.length; ++k) {
                if (k != i && k != j) {
                    rest.push(numbers[k])
                }
            }
            const target = [
                numbers[i] + numbers[j], numbers[i] - numbers[j],
                numbers[j] - numbers[i], numbers[i] * numbers[j]
            ]
            if (numbers[i]) target.push(numbers[j] / numbers[i]);
            if (numbers[j]) target.push(numbers[i] / numbers[j]);
            for (const t of target) {
                ans = ans || game24([t, ...rest])
                if (ans) return true
            }
        }
    }
    return ans
}

module.exports = data => {
    if (!data.numbers) {
        throw "input invalid" 
    }
    let numbers = data.numbers
    if (numbers.length !== 4 || !Array.isArray(numbers)) {
        throw "input invalid : numbers must array 4 values" 
    }
    for (let index = 0; index < numbers.length; index++) {
        if (numbers[index] > 9 || numbers[index] < 1) {
            throw "input invalid : value must more 0 and less than 10" 
        }
    }
    return game24(numbers) ? "YES" : "NO"
}
